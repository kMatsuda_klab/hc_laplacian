#ifndef vertex_hpp
#define vertex_hpp

#include <vector>
#include <Eigen/Core>

using namespace std;
using namespace Eigen;

class Vertex{
public:
	Vector3d org_loc;
	Vector3d current_loc;
	Vector3d previous_loc;
	Vector3d diff;

	vector<Vertex*> connect_vertices;
	bool fix = true;

	double alpha, beta;		// parameter for calculating laplacian and average difference

	Vertex(double _x, double _y, double _z, double _alpha, double _beta);
	void set_fix(bool _fix);
	void set_connection(Vertex* vp);

	void update();
	void calc_diff();
	void calc_modified_loc();
};

#endif