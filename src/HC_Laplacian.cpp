#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <direct.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "Vertex.hpp"

using namespace std;
using namespace Eigen;

vector<Vertex> org_vertices;
vector<Vector3i> org_facets;

vector<Vector3d> fix_vertices;


bool simple_laplacian_flag = false;	// If false, calculate HC-modified laplacian. If true, calculate simple laplacian.
double alpha = 0.0;
double beta = 0.0;
//double alpha = 0.5;
//double beta = 0.5;


// flag: true -> original, flag: false -> fix
void process_obj_line(string _line, bool _flag){
	stringstream ss;
	string v_f_flag;
	ss << _line;
	ss >> v_f_flag;
	if(v_f_flag == "v"){
		double data[3] = {0};
		for(int i = 0; i < 3; ++i){
			ss.ignore();
			ss >> data[i];
		}
		if(_flag){
			Vertex tmp(data[0], data[1], data[2], alpha, beta);
			org_vertices.push_back(tmp);
		}else{
			Vector3d tmp;
			tmp << data[0], data[1], data[2];
			fix_vertices.push_back(tmp);
		}
	}else if(v_f_flag == "f"){
		int data[3] = {0};
		for(int i = 0; i < 3; ++i){
			ss.ignore();
			ss >> data[i];
		}
		if(_flag){
			Vector3i tmp;
			tmp << data[0]-1, data[1]-1, data[2]-1;
			org_facets.push_back(tmp);
		}else{
			// nothing
		}
	}
}

void obj_loader(string folder_name, string _mesh_name, bool _flag){
	string buf;

	ifstream ifs("./"+folder_name+"/"+_mesh_name);
	if(!ifs){	cerr << "cannot open the input file!" << endl;	exit(1);	}

	while(getline(ifs, buf)){	process_obj_line(buf, _flag);	}

	ifs.close();
}

void output_obj(string _folder_name, string _mesh_name, int _count){
	ostringstream oss_count;
	oss_count.setf(ios::right);
	oss_count.fill('0');
	oss_count.width(4);
	oss_count << _count;

	ofstream ofs("./"+_folder_name+"/output/"+_mesh_name+"_"+oss_count.str()+".obj");
	if(!ofs){	cerr << "cannot make an output file!" << endl;	exit(1);	}

	for(int i = 0; i < org_vertices.size(); ++i){
		Vector3d tmp_current_loc = org_vertices[i].current_loc;
		ofs << "v " << tmp_current_loc.x() << " " << tmp_current_loc.y() << " " << tmp_current_loc.z() << endl;
	}

	for(int i = 0; i < org_facets.size(); ++i){
		ofs << "f " << org_facets[i].x()+1 << " " << org_facets[i].y()+1 << " " << org_facets[i].z()+1 << endl;
	}

	ofs.close();
}


void output_param(string _folder_name, string _org_name, string _fix_name, int _smoothing_max){
	ofstream ofs("./"+_folder_name+"/output/parameter.txt");
	if(!ofs){	cerr << "failed to output a setting file!" << endl;	exit(1);	}

	ofs << "simple laplacian flag: " << simple_laplacian_flag << endl;
	ofs << "alpha: " << alpha << endl;
	ofs << "beta : " << beta << endl;
	ofs << "org_name: " << _org_name << endl;
	ofs << "fix_name: " << _fix_name << endl;
	ofs << "times of smoothing: " << _smoothing_max << endl;
	ofs.close();
}

void set_connection(){
	for(int i = 0; i < org_facets.size(); ++i){
		Vector3i tmp = org_facets[i];
		Vertex* vp0 = &org_vertices[tmp.x()];
		Vertex* vp1 = &org_vertices[tmp.y()];
		Vertex* vp2 = &org_vertices[tmp.z()];

		vp0 -> set_connection(vp1);	vp0 -> set_connection(vp2);
		vp1 -> set_connection(vp2);	vp1 -> set_connection(vp0);
		vp2 -> set_connection(vp0);	vp2 -> set_connection(vp1);
	}
}

void set_move(){
	int count = 0;

	for(int i = 0; i < fix_vertices.size(); ++i){
		Vector3d tmp_f = fix_vertices[i];

		for(int j = 0; j < org_vertices.size(); ++j){
			Vector3d tmp_o = org_vertices[j].org_loc;

			Vector3d diff = tmp_f - tmp_o;
			double dist = diff.norm();
//			if(dist < 1e-3){
			if(dist < 1e-2){
				org_vertices[j].set_fix(false);
				++count;
				break;
			}

//			if((tmp_o.x() == tmp_f.x())&&(tmp_o.y() == tmp_f.y())&&(tmp_o.z() == tmp_f.z())){
//				org_vertices[j].set_fix(false);
//				count += 1;
//				break;
//			}
		}
	}

	cout << count << endl;
}

void simple_smoothing(){
	for(int i = 0; i < org_vertices.size(); ++i){
		org_vertices[i].update();
	}

	for(int i = 0; i < org_vertices.size(); ++i){
		org_vertices[i].calc_diff();
	}
}

void hc_smoothing(){
	for(int i = 0; i < org_vertices.size(); ++i){
		org_vertices[i].update();
	}

	for(int i = 0; i < org_vertices.size(); ++i){
		org_vertices[i].calc_diff();
	}

	for(int i = 0; i < org_vertices.size(); ++i){
		org_vertices[i].calc_modified_loc();
	}
}

int main(){
	string folder_name;
	string org_file_name;
	string fix_file_name;
	string smoothing_max_str;
	int smoothing_max;

	cout << "what is the folder name you want to use?: " << flush;
	cin >> folder_name;

	cout << "what is the file name you want to use for original mesh?: " << flush;
	cin >> org_file_name;

	cout << "what is the file name you want to use for moving?: " << flush;
	cin >> fix_file_name;

	cout << "how many times you want to apply the smoothing filter for the mesh?: " << flush;
	cin >> smoothing_max_str;
	stringstream smoothing_max_ss;
	smoothing_max_ss << smoothing_max_str;
	smoothing_max_ss >> smoothing_max;

	while(true){
		string laplacian_flag_answer;
		cout << "do you want to apply hc-modified laplacian filter? (if not, apply simple laplacian filter) (Y/n): " << flush;
		cin >> laplacian_flag_answer;
		if(laplacian_flag_answer == "Y"){
			simple_laplacian_flag = false;
			break;
		}else if(laplacian_flag_answer == "n"){
			simple_laplacian_flag = true;
			break;
		}
	}

	if(!simple_laplacian_flag){
		string alpha_str, beta_str;
		stringstream alpha_ss, beta_ss;
	
		cout << "alpha: " << flush;
		cin >> alpha_str;
		alpha_ss << alpha_str;
		alpha_ss >> alpha;
	
		cout << "beta: " << flush;
		cin >> beta_str;
		beta_ss << beta_str;
		beta_ss >> beta;
	}

	obj_loader(folder_name, org_file_name, true);
	obj_loader(folder_name, fix_file_name, false);
	cout << org_vertices.size() << endl;
	cout << fix_vertices.size() << endl;

	set_connection();
	set_move();
	string ofn = "./" + folder_name + "/output";
	_mkdir(ofn.c_str());
	output_param(folder_name, org_file_name, fix_file_name, smoothing_max);

	for(int i = 0; i < smoothing_max+1; ++i){
		if(simple_laplacian_flag){
			simple_smoothing();
		}else{
			hc_smoothing();
		}
		output_obj(folder_name, org_file_name, i);
	}
}