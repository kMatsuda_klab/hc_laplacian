#include <iostream>
#include <vector>
#include <Eigen/Core>

#include "Vertex.hpp"

using namespace std;
using namespace Eigen;


Vertex::Vertex(double _x, double _y, double _z, double _alpha, double _beta){
	org_loc << _x, _y, _z;
	current_loc = org_loc;
	previous_loc << 0.0, 0.0, 0.0;

	alpha = _alpha;
	beta = _beta;
}

void Vertex::set_connection(Vertex* vp){
	connect_vertices.push_back(vp);
}

void Vertex::set_fix(bool _fix){
//	fix = true;

	if(connect_vertices.size() == 0){
		// nothing
	}else{
		fix = _fix;
	}
}

void Vertex::update(){
	previous_loc = current_loc;
}

void Vertex::calc_diff(){
	if(fix){
		// nothing
	}else{
		// calculating laplacian
		current_loc << 0.0, 0.0, 0.0;
		diff << 0.0, 0.0, 0.0;

		int neighbour_size = connect_vertices.size();
		for(int i = 0; i < neighbour_size; ++i){
			Vertex* tmp_vp = connect_vertices[i];
			Vector3d tmp_previous_loc = tmp_vp -> previous_loc;
			current_loc += tmp_previous_loc;
		}
		current_loc /= double(neighbour_size);

		// calculating diff
		diff = current_loc - (alpha*org_loc + (1-alpha)*previous_loc);
	}
}

void Vertex::calc_modified_loc(){
	if(fix){
		// nothing
	}else{
		int neighbour_size = connect_vertices.size();
		Vector3d tmp_diff_avg;
		for(int i = 0; i < neighbour_size; ++i){
			Vertex* tmp_vp = connect_vertices[i];
			Vector3d tmp_diff = tmp_vp -> diff;
			tmp_diff_avg += tmp_diff;
		}
		tmp_diff_avg /= double(neighbour_size);

		// calculating modified position
		current_loc -= (beta*diff + (1-beta)*tmp_diff_avg);
	}
}